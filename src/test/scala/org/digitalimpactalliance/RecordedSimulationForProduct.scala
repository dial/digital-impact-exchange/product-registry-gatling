package org.digitalimpactalliance

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class RecordedSimulationForProduct extends Simulation {

  val httpProtocol = http
    .baseUrl("http://localhost:3000")
    .inferHtmlResources(
      BlackList(
        """.*\.js""",
        """.*\.css""",
        """.*\.gif""",
        """.*\.jpeg""",
        """.*\.jpg""",
        """.*\.ico""",
        """.*\.woff""",
        """.*\.woff2""",
        """.*\.(t|o)tf""",
        """.*\.png""",
        """.*detectportal\.firefox\.com.*""",
        """.*\.svg"""
      ),
      WhiteList()
    )
    .acceptHeader("*/*")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .userAgentHeader(
      "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:74.0) Gecko/20100101 Firefox/74.0"
    )

  val scn = scenario("RecordedSimulation")
  // Product Page
    .exec(
      http("products_index")
        .get("/products")
        .resources(
          http("workflows_count")
            .get("/workflows/count"),
          http("use_cases_count")
            .get("/use_cases/count"),
          http("organizations_count")
            .get("/organizations/count"),
          http("building_blocks_count")
            .get("/building_blocks/count"),
          http("products_count")
            .get("/products/count"),
          http("get_filters")
            .get("/get_filters"),
          http("sdgs_count")
            .get("/sustainable_development_goals/count")
        )
    )

  setUp(
    scn
      .inject(
        nothingFor(4 seconds),
        atOnceUsers(5),
        rampUsers(5) during (10 seconds),
        constantUsersPerSec(10) during (10 seconds),
        constantUsersPerSec(10) during (10 seconds) randomized,
        rampUsersPerSec(10) to 20 during (10 minutes),
        rampUsersPerSec(10) to 20 during (10 minutes) randomized,
        heavisideUsers(1000) during (20 seconds)
      )
      .protocols(httpProtocol)
  )
}
